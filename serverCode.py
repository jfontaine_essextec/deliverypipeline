from flask import Flask, request, jsonify
import json



app = Flask(__name__)


@app.route('/updateView', methods=['GET'])
def updateView():
	print('request came here')
	with open('./example.json') as f:
		data = json.load(f)
	return jsonify(data)


if __name__ == '__main__':
	print('server is starting')
	app.run(host='0.0.0.0', port=5001)