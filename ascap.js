


function getDataFromServer(){
	let update_file = 'https://bitbucket.org/jfontaine_essextec/deliverypipeline/raw/b458c9990da74188d7c2cf7a77fb7396678a629e/example.json'
	console.log(update_file);
	$.get(update_file,'json')
		.done(function(data){
			console.log(typeof JSON.parse(data));
			reloadCellContent(JSON.parse(data));
		});
}


function reloadCellContent(obj,parent){
	parent = parent || '';
	console.log('parent: ' + parent);
	var result = ''
	for(var key in obj){
		if( obj.hasOwnProperty(key) ){
			result += key + ' : ' + obj[key] + '\n';
			if(obj[key] instanceof Object){ // need to make a recursive call since it has object inside
				reloadCellContent(obj[key],key);
			}
			else {
				if( obj[key] != -1)
			  		$('.'+parent).find('.'+key).text(obj[key]);
			}
		}
	}
	console.log(result);

}

function buildContentObjectSingle(xLoc, yLoc,titleTextColor,titlefillColor, title, contentTextColor, contentFillColor, content){
	// console.log(title);
		$('body').append(
			$('<span></span>').append(
			$('<div></div>').text(title)
							.addClass('cell')
							.css({'color' : titleTextColor, 'background-color' : titlefillColor}),
			$('<div></div>').text(content)
							.addClass('cell')
							.css({'color' : contentTextColor, 'background-color' : contentFillColor })
	 		).css({top : xLoc, left : yLoc, position : 'absolute'})
	 		 .addClass('contentBox')
	 	);
}

$(document).ready(function(){

	var intervalID = 0;

	$('.refresh-button > .auto').click(function(){
		
		if ($(this).hasClass('off')) {
			$(this).text('Refresh On');

			intervalID = setInterval(getDataFromServer,1000);
		}
		else { // refresh must be on
			$(this).text('Refresh Off');
			clearInterval(intervalID)

		}

		$(this).toggleClass('off')
	});
	$('.refresh-button > .man').click(getDataFromServer);

});
